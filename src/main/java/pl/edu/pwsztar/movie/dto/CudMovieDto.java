package pl.edu.pwsztar.movie.dto;

import lombok.*;

@Builder(toBuilder = true)
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

public class CudMovieDto {
    private String title;
    private String videoId;
    private String image;
    private Integer year;

    @Override
    public String toString() {
        return "CudMovieDto{" +
                "title='" + title + '\'' +
                ", videoId='" + videoId + '\'' +
                '}';
    }
}
