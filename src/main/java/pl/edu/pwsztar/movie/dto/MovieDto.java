package pl.edu.pwsztar.movie.dto;

import java.io.Serializable;
import lombok.*;

@Builder(toBuilder = true)
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

public class MovieDto implements Serializable {
    private Long movieId;
    private String title;
    private String image;
    private Integer year;
}
