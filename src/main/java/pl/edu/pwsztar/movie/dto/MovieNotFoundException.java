package pl.edu.pwsztar.movie.dto;

public class MovieNotFoundException extends RuntimeException{
    public MovieNotFoundException(Long id) {
        super("Movie of "+id+"not found");
    }
}
