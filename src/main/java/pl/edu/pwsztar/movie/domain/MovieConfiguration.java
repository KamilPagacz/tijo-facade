package pl.edu.pwsztar.movie.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class MovieConfiguration {

    @Bean
    MovieFacade filmFacade(MovieRepository filmRepository) {
        MovieCreator filmCreator = new MovieCreator();
        return new MovieFacade(filmRepository, filmCreator);
    }
}
