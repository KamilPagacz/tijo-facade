package pl.edu.pwsztar.movie.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwsztar.movie.dto.CudMovieDto;
import pl.edu.pwsztar.movie.dto.MovieCounterDto;
import pl.edu.pwsztar.movie.dto.MovieDto;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
public class MovieFacade {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieFacade.class);
    private final MovieRepository movieRepository;
    private final MovieCreator movieCreator;

    public MovieFacade(MovieRepository movieRepository, MovieCreator movieCreator) {

        this.movieRepository = movieRepository;
        this.movieCreator = movieCreator;
    }

    public List<MovieDto> findAll() {
        return movieRepository.findAll().stream()
                .map(Movie::movieDto)
                .collect(Collectors.toList());
    }

    public void createMovie(CudMovieDto createMovieDto) {
        Movie movie = movieCreator.from(createMovieDto);
        movieRepository.save(movie);
    }

    public void deleteMovie(Long movieId) {
        movieRepository.deleteById(movieId);
    }

    public CudMovieDto findMovie(Long movieId) {
        Movie movie = movieRepository.findOneByMovieId(movieId);

        if(movie == null) {
            return CudMovieDto.builder().build();
        }

        return movie.cudMovieDto();
    }

    public MovieCounterDto countMovies() {
        return MovieCounterDto.builder().counter(movieRepository.count()).build();
    }

    public void updateMovie(Long movieId, CudMovieDto updateMovieDto) {
        Movie movie = movieRepository.findOneByMovieId(movieId);

        if(movie != null) {
            Movie movieBuilder = Movie.builder()
                    .movieId(movieId)
                    .image(updateMovieDto.getImage())
                    .title(updateMovieDto.getTitle())
                    .videoId(updateMovieDto.getVideoId())
                    .year(updateMovieDto.getYear())
                    .build();

            movieRepository.save(movieBuilder);
        }
    }
}
