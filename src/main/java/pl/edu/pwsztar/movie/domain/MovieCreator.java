package pl.edu.pwsztar.movie.domain;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.movie.dto.CudMovieDto;
import pl.edu.pwsztar.movie.dto.MovieDto;
import static java.util.Objects.requireNonNull;

@Component
public class MovieCreator {

    Movie from(MovieDto movieDto) {
        requireNonNull(movieDto);
        return Movie.builder()
                .movieId(movieDto.getMovieId())
                .title(movieDto.getTitle())
                .image(movieDto.getImage())
                .year(movieDto.getYear())
                .build();
    }

    Movie from(CudMovieDto CUDMovieDto){
        requireNonNull(CUDMovieDto);
        return Movie.builder()
                .title(CUDMovieDto.getTitle())
                .image(CUDMovieDto.getImage())
                .year(CUDMovieDto.getYear())
                .videoId(CUDMovieDto.getVideoId())
                .build();
    }
}
