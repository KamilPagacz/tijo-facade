package pl.edu.pwsztar.movie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.pwsztar.movie.domain.MovieFacade;
import pl.edu.pwsztar.movie.dto.CudMovieDto;
import pl.edu.pwsztar.movie.dto.MovieCounterDto;
import pl.edu.pwsztar.movie.dto.MovieDto;

import java.util.List;

@RestController
@RequestMapping(value="/api")
public class MovieController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieController.class);

    private final MovieFacade movieFacade;

    MovieController(MovieFacade movieFacade) {
        this.movieFacade = movieFacade;
    }

    @CrossOrigin
    @GetMapping(value = "/movies")
    public ResponseEntity<List<MovieDto>> getMovies() {
        LOGGER.info("find all movies");

        List<MovieDto> moviesDto = movieFacade.findAll();
        return new ResponseEntity<>(moviesDto, HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value = "/movies")
    public ResponseEntity<?> createMovie(@RequestBody CudMovieDto createMovieDto) {
        LOGGER.info("create movie: {}", createMovieDto);
        movieFacade.createMovie(createMovieDto);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(value = "/movies/{movieId}")
    public ResponseEntity<Void> deleteMovie(@PathVariable Long movieId) {
        LOGGER.info("delete movie: {}", movieId);
        movieFacade.deleteMovie(movieId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/movies/{movieId}")
    public ResponseEntity<CudMovieDto> detailsMovie(@PathVariable Long movieId) {
        LOGGER.info("details movie: {}", movieId);
        CudMovieDto CUDMovieDto = movieFacade.findMovie(movieId);

        return new ResponseEntity<>(CUDMovieDto, HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping(value = "/movies/{movieId}")
    public ResponseEntity<Void> updateMovie(@RequestBody CudMovieDto updateMovieDto,
                                            @PathVariable Long movieId) {
        LOGGER.info("update movie: {}", movieId);
        movieFacade.updateMovie(movieId, updateMovieDto);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/movies/counter")
    public ResponseEntity<MovieCounterDto> countMovies() {
        LOGGER.info("count movies");

        MovieCounterDto movieCounterDto = movieFacade.countMovies();
        return new ResponseEntity<>(movieCounterDto, HttpStatus.OK);
    }

}
